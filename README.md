# Arduino Motor Driver

This Arduino Program spins output (motors) up and down.
They are accelerated over a given amount of time, and automatically breaked in the same speed.
Best used with a Hardware Motor-Driver L293D.

Used to abstract rotation-speed from time needed for acceleration.

Wiring tbd.

This program does not block the cpu, by implementing tunning at defined timesteps.