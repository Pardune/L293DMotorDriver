/*
This program spins a motor up and down again.

*/

// Pin Declaration
const int driverOut01 = 10;
const int driverOut02 = 11;

/**
Variable Declaration
**/

// Limit maximum total Speed. 1..255
const unsigned int maxSpeed = 250;

// acceleration of Motors. In accel per second
unsigned int deltaSpin = 5;

// Time. Used to calculate millis
int previousMillis;
int currentMillis;



/**
Setup
**/
void setup() {
  
  previousMillis = 0;
  deltaSpin = 4;
}


// Write Motor speeds to outputs.
void out(int inSpeed1){
  analogWrite(driverOut01);
  analogWrite(driverOut02);
}



void fadeMotorSpeed(int seconds, int maxSpeed){
  
  /*
  spin motors up and down.
  
  Parameters:
    seconds		How many seconds the acceleration should increase.
    maxSpeed	Maximum Speed.
  */
  
  // proposed Motor rotate Speed
  int deltaSpin = maxSpeed/seconds;
  
  // pick Acceleration from smaller: deltaSpin || maxSpeed
  rotSpeed = (maxSpeed < deltaSpin ) maxSpeed | deltaSpin; 
  
  // store last run
  currentMillis = millis();

  if(currentMillis - previousMillis > seconds){
    //save last time;
    previousMillis = currentMillis;
    
    rotSpeed += deltaSpin;
    
    // see if direction should be changed
    if(rotSpeed >= 253 || rotSpeed <= 2){
      deltaSpin = -deltaSpin;
    }

    // set PWM Outputs
    out(rotSpeed);
  }
}

// Main Loop
void loop() {
  fadeMotorSpeed(time, maxSpeed);
}
